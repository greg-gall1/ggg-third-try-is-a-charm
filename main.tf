terraform {
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "~> 4.0"
    }
  }
}

provider "aws" {
  region = "us-east-2"
}



#setup object in the bucket.  this is my jar file in this example
resource "aws_s3_object" "object" {
  bucket = aws_s3_bucket.example.id
  key    = "g-hello-0.0.1-SNAPSHOT.jar"
  source = "build/libs/g-hello-0.0.1-SNAPSHOT.jar"
  source_hash = filemd5("build/libs/g-hello-0.0.1-SNAPSHOT.jar")
}

#setup bucket
resource "aws_s3_bucket" "example" {
  bucket = "ggg-teraform-fund-bucket"

  tags = {
    Name        = "ggg-terraform-bucket"
    Environment = "Dev"
  }
}

#set bucket policy
resource "aws_s3_bucket_policy" "allow_access_from_another_account" {
  bucket = aws_s3_bucket.example.id
  policy = data.aws_iam_policy_document.allow_access_from_another_account.json
}

### setup bucket ACL
resource "aws_s3_bucket_acl" "example" {
  bucket = aws_s3_bucket.example.id
  acl    = "private"
}

#add a bucket policy to allow get access to everyone
data "aws_iam_policy_document" "allow_access_from_another_account" {
  statement {
    principals {
      type        = "AWS"
      identifiers = ["*"]
    }

    actions = [
      "s3:GetObject",
      "s3:ListBucket",
    ]

    resources = [
      aws_s3_bucket.example.arn,
      "${aws_s3_bucket.example.arn}/*",
    ]
  }
}

#spins up spring boot instance:
resource "aws_instance" "app_server2" {
  ami                         = "ami-02238ac43d6385ab3"
  instance_type               = "t2.micro"
  security_groups             = [aws_security_group.ggg_spring_boot.name]
  key_name                    = "GGG"
  iam_instance_profile        = aws_iam_instance_profile.test_profile.id
  user_data = <<EOF
  #! /bin/bash
  yum update -y
  yum install -y java-17-amazon-corretto-headless
  aws s3api get-object --bucket ${aws_s3_bucket.example.bucket} --key ${aws_s3_object.object.key} ${aws_s3_object.object.key}
  java -jar ${aws_s3_object.object.key}
  EOF


  tags = {
    Name = var.instance_name2
  }
}

resource "aws_security_group" "ggg_spring_boot" {
  name        = "ggg_spring_boot"
  description = "Allow SSH inbound traffic"
  #vpc_id      = aws_vpc.main.id

  ingress {
    description = "SSH from anywhere"
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }
  ingress {
    description = "Spring Boot Port from anywhere"
    from_port   = 8080
    to_port     = 8080
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }
  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  tags = {
    Name = "ggg_spring_boot_security_group"
  }
}




resource "aws_iam_instance_profile" "test_profile" {
  name = "test_profile"
  role = aws_iam_role.test_role.name
}

##Test Role to use with the above Role Policy
resource "aws_iam_role" "test_role" {
  name = "ggg_test_role"

  assume_role_policy = jsonencode({
    Version = "2012-10-17"
    Statement = [
      {
        Action = "sts:AssumeRole"
        Effect = "Allow"
        Sid    = ""
        Principal = {
          Service = "ec2.amazonaws.com"
        }
      },
    ]
  })
}

##sets up I am role policy used by the apps to access the bucket, uses the test role defined below
resource "aws_iam_role_policy" "test_policy" {
  name = "ggg_test_policy"
  role = aws_iam_role.test_role.id

  # Terraform's "jsonencode" function converts a
  # Terraform expression result to valid JSON syntax.
  policy = jsonencode({
    "Version" : "2012-10-17",
    "Statement" : [
      {
        "Effect" : "Allow",
        "Action" : [
          "s3:GetObject",
          "s3:ListBucket"
        ],
        "Resource" : [
          "${aws_s3_bucket.example.arn}",
          "${aws_s3_bucket.example.arn}/*"
        ]
      },
      {
        "Effect" : "Allow",
        "Action" : "s3:ListAllMyBuckets",
        "Resource" : "*"
      }
    ]
  })
}
