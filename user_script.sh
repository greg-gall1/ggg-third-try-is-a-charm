<<EOF
#!/bin/bash
sudo yum update -y 
sudo yum install -y java-17-amazon-corretto-headless
aws s3api get-object --bucket ${aws_s3_bucket.example.bucket} --key ${aws_s3_object.object.key} ${aws_s3_object.object.key}
java -jar ${aws_s3_object.object.key}
EOF