variable "instance_name" {
  description = "Value of the Name tag for the EC2 instance"
  type        = string
  default     = "GGGPostGresInstance"
}

variable "instance_name2" {
  description = "Value of the Name tag for the EC2 instance"
  type        = string
  default     = "GGGSpringBoot"
}